#ifndef CLASSES_H
#define CLASSES_H

#include <RcppEigen.h>
#include "classes.cpp"

using namespace Rcpp;
using namespace Eigen;

enum PADTYPE{EVEN, ODD, NONE};
enum PADSIDE{LEFT, RIGHT};

/*
 * Multivariate Time Series class
 * 
 * Columns are channels.  Ideally, as much processing as possible can be done
 * within Rcpp using pointers to the MatrixXd object, data.  This way, we can 
 * greatly reduce the memory footprint of processing operations.
 */
class Mts{
  
  public:
    MatrixXd &data;
    int channels;  // # of channels
    int samples;   // 
      
    Mts(MatrixXd &x);
};


/* 
 * Padded Multivariate Time Series class
 * 
 * For padded operations (like filtering), the padding will be saved as separate 
 * matrices, so we don't have to reallocate our data matrix.
 * 
 * Following Scipy, padding can be NONE, EVEN, or ODD.  NONE creates padding 
 * filled with zeros; EVEN results in padding which is a reflection of the start 
 * or end of the signal, and ODD is the inverted reflection (i.e. a 180 degree 
 * rotation around the start and end of the signal).
 */
class MtsPadded{
  
  public:
    // Members
    Mts &mts;
    MatrixXd pad_left;
    MatrixXd pad_right;
    int pad_size;
    int samples;
    
    // Methods
    // Indexing for the padded matrix.
    double operator()(int n, int m);
    
    MtsPadded(Mts &mts, int pad_size, PADTYPE type);
};

/* 
 * ====== Class init helper functions
 */

/* 
 * Fill a left or right padding matrix with samples from signal.
 */
void fill_padding(MatrixXd &padding, Mts &mts, PADSIDE side);

#endif