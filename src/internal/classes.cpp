using namespace Rcpp;
using namespace Eigen;

#include "classes.h"

/*
 * ======================== Class constructors =================================
 */

inline Mts::Mts(MatrixXd &x): data(x) {
  
  channels = data.cols();
  samples = data.rows();
  
}

inline MtsPadded::MtsPadded(Mts &x, int pad_size, PADTYPE type): mts(x) {

  // Add members to object  
  MatrixXd pad_left(pad_size, mts.data.cols());
  MatrixXd pad_right(pad_size, mts.data.cols());
  samples = mts.data.rows() + pad_size;
   
  // Init padding to zeros
  pad_left.setZero();
  pad_right.setZero();
  
  
  switch (type){
    case NONE:
      // Do nothing, pads already init to 0
      break;
    case EVEN:
      // Copy reflected signal as padding
      fill_padding(pad_left, mts, LEFT);
      fill_padding(pad_left, mts, RIGHT);
      break;
    case ODD:
      // Copy inverse of reflected signal as padding
      fill_padding(pad_left, mts, LEFT);
      fill_padding(pad_left, mts, RIGHT);
      pad_left *= -1;
      pad_right *= -1;
      break;
  }
  
  #ifdef DEGUG
  PRINTMATRIX("Padding left", pad_left);
  PRINTMATRIX("Padding right", pad_right);
  #endif
}
 
void fill_padding(MatrixXd &padding, Mts &mts, PADSIDE side){
  
  int pad_size = padding.rows();
  // Number of padding values top take from signal.
  int n_extract = std::min(pad_size, mts.samples);
 
  switch (side){
    case LEFT:
      padding = mts.data.block(
        pad_size - n_extract, 
        0, 
        n_extract, 
        mts.channels);
      break;
    case RIGHT:
      padding = mts.data.block(
        0, 
        0, 
        n_extract, 
        mts.channels);
      break;
  }
} 
  


